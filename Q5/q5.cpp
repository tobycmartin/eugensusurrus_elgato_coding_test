#include <QString>
#include <QDebug>

void printLastCharacterUppercase( )
{
	Q_UNUSED(argc)
	Q_UNUSED(argv)
	QString hello = "Hello";
	const char *lastCharacter = nullptr;
		for (int i = 0; i < hello.length(); ++i)
		{
			auto localVariable = hello[i];
			localVariable = localVariable.toUpper();
			qDebug() << "Character: " << localVariable;
			QByteArray characterByte = QString(localVariable).toUtf8();
			lastCharacter = characterByte.constData();
		}
	Q_ASSERT(hello == "Hello");
	qDebug() << "Last character in string: " << lastCharacter;
}

// Though I'm not that familiar with QT, I know that it is a framework for developing crossplatform applicatins.
// The functio seems to be changing the string Hello to upper case character by character in the for loop
// so at the end of the execution I'm expecting the Qstring hello = "HELLO", so a failed assertion and the 
// last character should be also nullptr since every string is terminated in a null character.
// I think the same result can be achieved using the toUpper function on the strig itself like

// hello = helo.toUpper();
