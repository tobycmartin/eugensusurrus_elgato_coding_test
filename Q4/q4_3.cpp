#include <iostream>
#include <ctime>

/** Public API functions */
/*
Function:		  API_FormatDateTimeString
Description:	API Interface function to reformat a US format
				      date/time string MM/DD/YYYY into the format Month DD,YYYY
Released: 		Interface made public in release v1.01 on 01.01.2019.
				      Interface changes are not permissible.
Returns: 		  A pointer to the date/time string
*/

const char * API_FormatDateTimeString(const char * inDate)
{
  struct tm date;
  // The function returns a danling pointer. In order to avoid this we need to allocate
  // dynamic memory and return the pointer to the dynamic memory.
	char* formatted_date = new char[60];
	
	strptime( inDate, "%m/%d/%Y", &date );
	strftime( formatted_date, 68, "%B %d, %Y", &date );
	debug_console_puts( formatted_date );
	return (const char *) formatted_date;
}

// This should work

int main() {

  const char* source_date = "04/27/2020"; 
  const char* dest_date = API_FormatDateTimeString(source_date);

  std::cout << source_date <<"\n";  // 04/27/2020
  std::cout << dest_date << "\n";   // April 27, 2020

  return 0;
}
