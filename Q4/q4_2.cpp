char const * const SomeString = "A C coding test during an interview at Elgato";
// declaration and initialization of a
// constant pointer to a constant C style string. Impossible to change the string nor the pointer

std::size_t Length = strlen(SomeString);
// declaration and initialization of a
// variable Length of size type, equals the length of C-Style SomeString without the null terminator
char * Dest = (char *) malloc(Length);
// allocates a memory block of size Length and returns a pointer of type
// void* which is then casted to type char*. Dest now points to the beginning of the allocated memory block
strcpy(Dest, SomeString);

// copies the contents of SomeString in the previously allocated memory block which is pointed at by Dest
// now Dest has the same value as SomeString but it's contents can be changed
