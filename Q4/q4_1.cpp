#define MIN(a,b) a < b ? a : b

// This is a macro or a preprocessor directive in c++ and it will preprocess the expression before compiling.
// The preprocessor directies are considered a dangerous practice in C++. This specific one uses the ternary 
// conditional operator which will assign the value a if a < b or b otherwise. In other words it will assign
// the minimum values of the two.
