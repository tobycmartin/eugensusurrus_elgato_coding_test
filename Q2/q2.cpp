// main.cpp

#include "matrix.h"
#include <string>

int main()
{
	const size_t rows = 3;
	const size_t cols = 3;
	
	matrix<std::string, rows, cols> m;
	for (size_t row = 0; row < rows; row++)
		for (size_t col = 0; col < cols && col <= row; col++)
			m[row][col] = std::to_string(row) + "," + std::to_string(col);
		
	// for example in this case this line would access out of bounds memory and modify it without any warnings
    	// causing unstable behavior
    	// m[8][10] = std::string("random");
	
	return 0;
}
