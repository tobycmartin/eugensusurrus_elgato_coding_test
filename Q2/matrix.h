// matrix.h ; rev. 1

#ifndef _H_MATRIX_
#define _H_MATRIX_

#include <array>

template <typename T, size_t NR, size_t NC>
	class matrix
	{
	private:
		using row_container_t = std::array<T, NC>;
		using container_t = std::array<row_container_t, NR>;
		class row_proxy
		{
		public:
			row_proxy(row_container_t& irow_container) : row_container(irow_container) {}
// it does not return an out of range exception and elements outside the allocated memory bounds can be accessed
// a fix would be writing the overloaded operator[] with making the use of std::array::at() function
// which would throw an out of bounds exception
			T& operator[](size_t col) const { return row_container.at(col); }
	
		private:
			row_container_t& row_container;
		};
		
	public:
		// same here
		row_proxy operator[](size_t row) { return row_proxy(container.at(row)); }
		
		void clear()
		{
			container_t dummy;
			container.swap(dummy);
		}
		
	private:
		container_t container;
	};
	
#endif // _H_MATRIX_
