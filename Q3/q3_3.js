let state;
if(state == null) {
  // Do something...
}

// The variable state is an undefined variable which has the undefined type but the null is an object. In this case the 
// if statement will execute because the coparison is not type strong / type matched (===). A way to fix this would be to use
// the strict comparison operator.

let state;
if(state === null) {
  // Do something...
}
