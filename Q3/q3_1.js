var i = 5;
for(var i = 0; i < 10; i++) {
  // Do something...
}
console.log(i); // outputs 10

// this happens because in javascript all the variable declarations, no matter where declared will move at the top of their scope.
// In this case it is going to be global scope and the var i = 0 inside the for loop actually has the same scope as the var i = 5,
// hence it will overwrite it to 0 and then increment it inside the loop to 10

// a way to specify that the variable reffers only to the current block would be to use the let key word instead of var. something like this

var i = 5;
for(let i = 0; i < 10; i++) {
  // Do something...
}
console.log(i); // outputs 5 since let i has only scope within the for block
