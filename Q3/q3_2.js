Game.prototype.restart = function () {
  this.timer = setTimeout(function() {
    this.clearBoard();
  }, 1000);
};

// In javascript this refers to the parent object and it ma happen that the context of the clearBoard function could
// be of a different element than expected. Hence we need to make sure that it allways refers to the Game object.
// A good idea is to save the this reference in a variable

Game.prototype.restart = function() {
  var parent_reference = this;
  this.timer = setTimeout(function() {
    parent_reference.clearBoard();
  }, 1000);
}
