#include <vector>
#include <cassert>
#include <iterator>
#include <algorithm>


int myFunction(std::vector<int> vector1, std::vector<int> vector2)
{
/*
Counts the number of elements in vector1 which are less or equal than all of the
elements in vector vector2
*/

    // Improved time complexity from N*M to NlogN

    std::sort(vector1.begin(), vector1.end());      //NlogN time
    std::sort(vector2.begin(), vector2.end());      //NlogN time

    std::vector<int>::iterator first_greater = std::upper_bound(vector1.begin(), vector1.end(), *vector2.begin());  //logN time
    int num_elem_less_that_all_in_v2 = std::distance(vector1.begin(), first_greater);   // N time

    return num_elem_less_that_all_in_v2;
}

int main()
{
    //Additional test cases
    assert(myFunction(std::vector<int>{1, 2, 3}, std::vector<int>{2, 3, 4}) == 2 && "Expected 2");
    assert(myFunction(std::vector<int>{2, 2, 3}, std::vector<int>{2, 3, 4}) == 2 && "Expected 2");
    assert(myFunction(std::vector<int>{3, 2, 3}, std::vector<int>{2, 3, 4}) == 1 && "Expected 1");
    assert(myFunction(std::vector<int>{}, std::vector<int>{2, 3, 4}) == 0 && "Expected 0");
    assert(myFunction(std::vector<int>{}, std::vector<int>{}) == 0 && "Expected 0");
    return 0;
}
